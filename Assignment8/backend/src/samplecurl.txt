curl -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im53b25nQG5vdGhpbmcuY29tIiwiZW1haWwiOiJud29uZ0Bub3RoaW5nLmNvbSIsImlhdCI6MTYzODQ5Nzk2NSwiZXhwIjoxNjM4NDk5NzY1fQ.amp-QxWY0k7ChycLwxsZe3CcFlodBojCVV8-6p0huRI'

curl -X 'GET' \
  'http://localhost:3010/v0/listings/users?username=nwong%40nothing.com' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im53b25nQG5vdGhpbmcuY29tIiwiZW1haWwiOiJud29uZ0Bub3RoaW5nLmNvbSIsImlhdCI6MTYzODQ5Nzk2NSwiZXhwIjoxNjM4NDk5NzY1fQ.amp-QxWY0k7ChycLwxsZe3CcFlodBojCVV8-6p0huRI'


curl -X 'GET' \
  'http://localhost:3010/v0/listings/users?username=nwong%40nothing.com' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im53b25nQG5vdGhpbmcuY29tIiwiZW1haWwiOiJud29uZ0Bub3RoaW5nLmNvbSIsImlhdCI6MTYzODQ5Nzk2NSwiZXhwIjoxNjM4NDk5NzY1fQ.amp-QxWY0k7ChycLwxsZe3CcFlodBojCVV8-6p0huRI'


  await request.post('/v0/auth')
      .send(listingLoginBody)
      .expect(200)
      .then(async (data) => {
        const accessToken = data.body.accessToken;

test('POST new Listing with Auth', async () => {
  await request.post('/v0/auth')
      .send(listingLoginBody)
      .expect(200)
      .then(async (data) => {
        const accessToken = data.body.accessToken;
        await request.post('/v0/listings')
          .send(listing)
          .set('Authorization', `Bearer ${accessToken}`)
          .expect(201)
          .then((data) => {
          //  console.log(data)
            expect(data).toBeDefined();
            expect(data.body).toBeDefined();
            expect(data.body.city).toEqual('Sigtuna');
            expect(data.body.descript).toEqual('Volvo V90');
            expect(data.body.username).toEqual('crl@hotmail.com');
          });
      });
});