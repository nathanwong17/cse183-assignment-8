const jwt = require('jsonwebtoken');
const db = require('./db');

// put this in properties file
const secretToken = 'test123';

exports.authenticate = async (req, res) => {
  const {username, password} = req.body;
  const account = await db.getAccountByUsername(username);
  if (account !== undefined && password === account.passwordhash) {
    const accessToken = jwt.sign(
      {username: account.username, email: account.email},
      secretToken, {
        expiresIn: '30m',
        algorithm: 'HS256',
      });
    account.accessToken = accessToken;
    res.status(200).json(account);
  } else {
    res.status(401).send('Username or password incorrect');
  }
};

exports.check = (req, res, next) => {
  const authHeader = req.headers.authorization;
  if (authHeader) {
    const token = authHeader.split(' ')[1];
    jwt.verify(token, secretToken, (err, user) => {
      if (err) {
        return res.sendStatus(403);
      }
      req.user = user;
      next();
    });
  } else {
    res.sendStatus(401);
  }
};
