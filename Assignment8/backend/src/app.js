const express = require('express');
const cors = require('cors');
const yaml = require('js-yaml');
const swaggerUi = require('swagger-ui-express');
const fs = require('fs');
const path = require('path');
const OpenApiValidator = require('express-openapi-validator');

// const dummy = require('./dummy');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));

const apiSpec = path.join(__dirname, '../api/openapi.yaml');

const apidoc = yaml.load(fs.readFileSync(apiSpec, 'utf8'));
app.use('/v0/api-docs', swaggerUi.serve, swaggerUi.setup(apidoc));

app.use(
  OpenApiValidator.middleware({
    apiSpec: apiSpec,
    validateRequests: true,
    validateResponses: true,
  }),
);

// app.get('/v0/dummy', dummy.get);

// AUTH.JS
// -----------------------------------------------------------------
const auth = require('./auth');
app.post('/v0/auth', auth.authenticate);
// -----------------------------------------------------------------


// Your routes go here

// LISTINGS.JS
// -----------------------------------------------------------------
const listings = require('./listings');
app.get('/v0/listings', listings.getAllListings);
// app.post('/v0/listings', listings.newListings);
app.get('/v0/listings/search', listings.getListingByKeyword);
app.get('/v0/users', listings.getAllUsers );
app.post('/v0/users', listings.newUser);
//  app.get('/v0/cata', listings.getAllcatagories);

// ------------------ PROTECTED ROUTES -----------------------------
app.post('/v0/listings', auth.check, listings.newListings);
app.get('/v0/listings/users', auth.check, listings.getOwnListings);
app.post('/v0/listings/response', auth.check, listings.postResponse);
app.get('/v0/listings/response/:id', auth.check, listings.getResponsesById);
// -----------------------------------------------------------------

// CATEGORY.JS
// -----------------------------------------------------------------
const category = require('./category');
// const {AjvOptions} =
// require('express-openapi-validator/dist/framework/ajv/options');
app.get('/v0/category', category.getCategoryByPath);
app.get('/v0/category/search', category.getSearchKeyWords);
// -----------------------------------------------------------------

app.use((err, req, res, next) => {
  res.status(err.status).json({
    message: err.message,
    errors: err.errors,
    status: err.status,
  });
});

module.exports = app;
