const supertest = require('supertest');
const http = require('http');

const db = require('./db');
const app = require('../app');

let server;

beforeAll(() => {
  server = http.createServer(app);
  server.listen();
  request = supertest(server);
  return db.reset();
});

afterAll((done) => {
  server.close(done);
});


const body = {
  'username': 'crl@hotmail.com',
  'password': 'foobar'};

test('POST Auth', async () => {
  await request.post('/v0/auth')
    .send(body)
    .expect(200)
    .then((data) => {
      //  console.log(data)
      expect(data).toBeDefined();
      expect(data.body).toBeDefined();
      expect(data.body.fname).toEqual('Carl');
      expect(data.body.lname).toEqual('Ramfelt');
      expect(data.body.username).toEqual('crl@hotmail.com');
    });
});

const body1 = {
  'username': 'crAAAAAA@hotmail.com',
  'password': 'foobar'};

test('POST Auth', async () => {
  await request.post('/v0/auth')
    .send(body1)
    .expect(401);
});

test('GET all listings', async () => {
  await request.get('/v0/listings?catagory=%2Fmarketplace')
    .expect(200)
    .then((data) => {
      expect(data).toBeDefined();
      expect(data.body).toBeDefined();
      expect(data.body[0].id).toEqual('16');
      expect(data.body[1].price).toEqual('599');
      expect(data.body[2].city).toEqual('Santa Clara');
    });
});

test('GET all car listings', async () => {
  await request.get('/v0/listings?catagory=%2Fmarketplace%2Fcars')
    .expect(200)
    .then((data) => {
      expect(data).toBeDefined();
      expect(data.body).toBeDefined();
      expect(data.body[0].descript).toEqual('1986 Ferrari 328 GTS37K');
      expect(data.body[1].descript).toEqual('2012 Mclaren MP4-12C Coupe 2D');
      expect(data.body[2].descript).toEqual('2012 Mclaren MP4-12C Coupe 2D');
    });
});

const listing = {
  'price': '10000',
  'descript': 'Volvo V90',
  'img': 'img',
  'city': 'Sigtuna',
  'state': 'Sweden',
  'milage': '10km',
  'catagory': '/marketplace/cars',
  'title': 'Volvo car to Sell',
  'username': 'crl@hotmail.com',
};

const listingLoginBody = {
  'username': 'crl@hotmail.com',
  'password': 'foobar'};

test('POST new Listing with Auth', async () => {
  await request.post('/v0/auth')
    .send(listingLoginBody)
    .expect(200)
    .then(async (data) => {
      const accessToken = data.body.accessToken;
      await request.post('/v0/listings')
        .send(listing)
        .set('Authorization', `Bearer ${accessToken}`)
        .expect(201)
        .then((data) => {
        //  console.log(data)
          expect(data).toBeDefined();
          expect(data.body).toBeDefined();
          expect(data.body.city).toEqual('Sigtuna');
          expect(data.body.descript).toEqual('Volvo V90');
          expect(data.body.username).toEqual('crl@hotmail.com');
        });
    });
});

// test('POST new Listing no Auth', async () => {
//   await request.post('/v0/listings')
//       .send(listing)
//       .expect(401)
// });

test('GET listings by keyword', async () => {
  await request.get('/v0/listings/search?words=Ford')
    .expect(200)
    .then((data) => {
      expect(data).toBeDefined();
      expect(data.body).toBeDefined();
      expect(data.body[0].descript).toEqual('1931 Ford Aerostar');
      expect(data.body[1].descript).toEqual('1931 Ford Aerostar');
      expect(data.body[2].descript).toEqual('1931 Ford Aerostar');
    });
});

test('GET listings by user', async () => {
  await request.post('/v0/auth')
    .send(listingLoginBody)
    .expect(200)
    .then(async (userData) => {
      const accessToken = userData.body.accessToken;
      await request.get('/v0/listings/users?username=crl%40hotmail.com')
        .set('Authorization', `Bearer ${accessToken}`)
        .expect(200)
        .then((data) => {
          expect(data).toBeDefined();
          expect(data.body).toBeDefined();
          expect(data.body[0].descript).toEqual('Computer Desk');
          expect(data.body[1].descript).toEqual('Gaming Chair');
          expect(data.body[2].descript)
            .toEqual('2017 Mercedes-Benz C-Class AMG C 63 S');
        });
    });
});

const repo = {
  'id': 10,
  'fname': 'Carl',
  'lname': 'Ramfelt',
  'listing_id': '12',
  'username': 'crl@hotmail.com',
  'msg': 'Can I buy?',
  'createtime': '2021-12-02T02:20:51.750Z',
};

test('POST new response', async () => {
  await request.post('/v0/auth')
    .send(listingLoginBody)
    .expect(200)
    .then(async (data) => {
      const accessToken = data.body.accessToken;
      await request.post('/v0/listings/response')
        .send(repo)
        .set('Authorization', `Bearer ${accessToken}`)
        .expect(201);
    });
});

test('GET responses', async () => {
  await request.post('/v0/auth')
    .send(listingLoginBody)
    .expect(200)
    .then(async (userData) => {
      const accessToken = userData.body.accessToken;
      await request.get('/v0/listings/response/15')
        .set('Authorization', `Bearer ${accessToken}`)
        .expect(200)
        .then((data) => {
          expect(data).toBeDefined();
          expect(data.body).toBeDefined();
          expect(data.body[0].username).toEqual('nwong@nothing.com');
          expect(data.body[0].fname).toEqual('Nathan');
          expect(data.body[0].lname).toEqual('Wong');
        });
    });
});

test('GET all users', async () => {
  await request.get('/v0/users')
    .expect(200)
    .then((data) => {
      expect(data).toBeDefined();
      expect(data.body).toBeDefined();
      expect(data.body[0].fname).toEqual('Carl');
      expect(data.body[1].fname).toEqual('Hilde');
      expect(data.body[2].passwordhash).toEqual('foobar2');
    });
});

test('GET all paths', async () => {
  await request.get('/v0/category')
    .expect(200)
    .then((data) => {
      expect(data).toBeDefined();
      expect(data.body).toBeDefined();
      expect(data.body.cat_name).toEqual('Marketplace');
      expect(data.body.cat_path).toEqual('/marketplace');
    });
});

const pathData = {
  'cat_path': '/marketplace',
  'cat_name': 'Marketplace',
  'parts': [
    {
      'cat_path': '/marketplace',
      'cat_name': 'Marketplace',
    },
  ],
  'children': [
    {
      'cat_path': '/marketplace/boats',
      'cat_name': 'Boats',
    },
    {
      'cat_path': '/marketplace/cars',
      'cat_name': 'Cars',
    },
    {
      'cat_path': '/marketplace/electronics',
      'cat_name': 'Electronics',
    },
    {
      'cat_path': '/marketplace/furniture',
      'cat_name': 'Furniture',
    },
    {
      'cat_path': '/marketplace/motorbikes',
      'cat_name': 'Motorbikes',
    },
    {
      'cat_path': '/marketplace/planes',
      'cat_name': 'Planes',
    },
  ],
};

test('GET path /marketplace', async () => {
  await request.get('/v0/category?path=%2Fmarketplace')
    .expect(200)
    .then((data) => {
      expect(data).toBeDefined();
      expect(data.body).toBeDefined();
      expect(data.body).toEqual(pathData);
    });
});

test('GET path and Children of catagory', async () => {
  await request.get('/v0/category?path=%2Fmarketplace%2Fcars')
    .expect(200)
    .then((data) => {
      expect(data).toBeDefined();
      expect(data.body).toBeDefined();
      expect(data.body.cat_name).toEqual('Cars');
      expect(data.body.parts[0].cat_name).toEqual('Marketplace');
      expect(data.body.parts[1].cat_path).toEqual('/marketplace/cars');
    });
});

test('GET search words', async () => {
  await request.get('/v0/category/search?words=a')
    .expect(200)
    .then((data) => {
      expect(data).toBeDefined();
      expect(data.body).toBeDefined();
      expect(data.body[0]).toEqual('Ferrari');
      expect(data.body[1]).toEqual('Mclaren');
    });
});

const user = {
  'fname': 'Ryan',
  'lname': 'Brother',
  'email': 'rbrother@brother.com',
  'username': 'rbrother@brother.com',
  'passwordhash': 'iAmBrother',
};

test('POST new user', async () => {
  const date = new Date();
  const time = date.getTime();
  user.username = `${time}@test.com`;
  user.email = `${time}@test.com`;
  await request.post('/v0/users')
    .send(user)
    .expect(201)
    .then((data) => {
      expect(data).toBeDefined();
      expect(data.body).toBeDefined();
      expect(data.body.fname).toEqual('Ryan');
      expect(data.body.username).toEqual(user.email);
    });
});

test('Invalid Copy POST new user', async () => {
  await request.post('/v0/users')
    .send(user)
    .expect(401);
});

test('App Error', async () => {
  await request.get('/v0/categ')
    .expect(404);
});

test('Invalid Token', async () => {
  await request.post('/v0/listings')
    .send(listing)
    .set('Authorization', `Bearer 343254343653254436`)
    .expect(403);
});

test('POST new Listing unauthorized', async () => {
  await request.post('/v0/auth')
    .send(listingLoginBody)
    .expect(200)
    .then(async (data) => {
      await request.post('/v0/listings')
        .send(listing)
        .expect(401);
    });
});
