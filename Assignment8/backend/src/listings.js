const db = require('./db');
const jwt = require('jsonwebtoken');

exports.getAllListings = async (req, res) => {
  const listings = await db.getListingsByCategory(req.query.catagory);
  res.status(200).json(listings);
};

exports.getAllUsers = async (req, res) => {
  const users = await db.getAllU();
  res.status(200).json(users);
};

exports.newListings = async (req, res) => {
  await db.postNewListing(req.body);
  res.status(201).json(req.body);
};

exports.newUser = async (req, res) => {
  const reply = await db.postNewUser(req.body);
  if (reply === 1) {
    res.status(401).send();
  } else {
    const newAccount = {...req.body};
    const token = jwt.sign(
      {username: newAccount.username, email: newAccount.email},
      'test123', {
        expiresIn: '30m',
        algorithm: 'HS256',
      });
    newAccount.accessToken = token;
    res.status(201).json(newAccount);
  }
};

// need function to get listing by searching keywords
// add path to the yaml file
// add path to app.js
exports.getListingByKeyword = async (req, res) => {
  const listings = await db.getListingByKeyword(req.query.words);
  res.status(200).json(listings);
};

/**
 * Function to get the user's listings
 * @param {*} req
 * @param {*} res
 */
exports.getOwnListings = async (req, res) => {
  const listings = await db.getOwnListings(req.query.username);
  res.status(200).json(listings);
};

exports.postResponse = async (req, res) => {
  await db.postResponse(req.body);
  res.status(201).send();
};

exports.getResponsesById = async (req, res) => {
  const responses = await db.getResponsesByListingId(req.params.id);
  res.status(200).json(responses);
};
