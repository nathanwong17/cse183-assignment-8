const {Pool} = require('pg');

const pool = new Pool({
  host: 'localhost',
  port: 5432,
  database: 'dev',
  user: 'postgres',
  password: 'postgres',
});

exports.postNewListing = async (listing) => {
  const date = new Date();
  const id = date.getTime();
  let insert =
    `INSERT INTO listing
    (id,price, descript, img, city, stat, milage, catagory, title, username)`;
  insert+= 'VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)';
  const query = {
    text: insert,
    values: [id, listing.price, listing.descript,
      listing.img, 'Santa Cruz', 'CA', '100',
      listing.catagory, listing.title, listing.username],
  };
  await pool.query(query);
};

exports.getAllU = async () => {
  const select =
    'SELECT fname, lname, email, username, passwordHash FROM account';
  const query = {
    text: select,
    values: [],
  };
  const {rows} = await pool.query(query);
  //    console.log(rows);
  return rows;
};

exports.postNewUser = async (account) => {
  // check for if email account already in use or username in use
  const insert = 'SELECT email, username from account';
  const query = {
    text: insert,
    values: [],
  };
  const {rows} = await pool.query(query);
  console.log(rows);
  for (let i = 0; i < rows.length; i++) {
    if (rows[i].email === account.email ||
       rows[i].username === account.username) {
      //  console.log("Invalid username or account used");
      return 1;
    }
  }
  // Account can be created without any conflicts
  let insert2 = `INSERT INTO account
  (fname, lname, email, username, passwordHash)`;
  insert2+= 'VALUES ($1,$2,$3,$4,$5)';
  const query2 = {
    text: insert2,
    values: [account.fname, account.lname,
      account.email, account.username, account.passwordhash],
  };
  await pool.query(query2);
};

/**
 * Function to get a category by path
 * @param {*} path the desired path
 * @return {*} the category's path and name
 */
exports.getCategoryByPath = async (path) => {
  const select = `SELECT * from category WHERE cat_path = $1`;
  const query = {
    text: select,
    values: [path],
  };
  const {rows} = await pool.query(query);
  return rows[0];
};

/**
 * Function to get the path parts to display as breadcrumbs
 * @param {*} path the path to get from
 * @return {*} an array of paths
 */
function getPathParts(path) {
  const pathParts = path.split('/');
  let currentPath = '';
  const paths = [];
  for (const part of pathParts) {
    if (part.length > 0) {
      currentPath = currentPath.concat('/', part);
      paths.push(currentPath);
    }
  }
  return paths;
}

/**
 * Function to convert an array to a string
 * @param {*} a the element to convert
 * @return {*} the array as a string
 */
function convertToString(a) {
  let str = '';
  for (let i = 0; i < a.length; i++) {
    if (i > 0) {
      str = str.concat(', ');
    }
    str = str.concat(`'`, a[i], `'`);
  }
  return str;
}

/**
 * Function to return a path and its parts
 * @param {*} path the path to get from
 * @return {*}
 */
exports.getCategoryParts = async (path) => {
  const values = getPathParts(path);
  const listString = convertToString(values);
  const select =
    `SELECT * FROM category WHERE cat_path IN (${listString})
    ORDER BY cat_path`;
  const query = {
    text: select,
    values: [],
  };
  const {rows} = await pool.query(query);
  return rows;
};

/**
 * Function to get the children of the path taken
 * @param {*} path the path to take
 * @return {object} an object containing children
 */
exports.getChildren = async (path) => {
  const select = `SELECT * FROM category WHERE cat_path ~* '^${path}/[^/]+$'
    ORDER BY cat_path`;
  const query = {
    text: select,
    values: [],
  };
  const {rows} = await pool.query(query);
  return rows;
};

/**
 * Function to get all the listings by category path
 * @param {*} path the path to take
 * @return {object} an object containing all the listings of the category
 */
exports.getListingsByCategory = async (path) => {
  const select = `SELECT * FROM listing WHERE catagory ~* '^${path}'`;
  const query = {
    text: select,
    values: [],
  };
  const {rows} = await pool.query(query);
  return rows;
};

exports.getListingByKeyword = async (keyword) => {
  const select = `SELECT * FROM listing WHERE descript ~* $1 OR title ~* $2`;
  const query = {
    text: select,
    values: [keyword, keyword],
  };
  const {rows} = await pool.query(query);
  return rows;
};

exports.getKeywords = async (keyword) => {
  const select = `SELECT * FROM keywords WHERE keyword ~* $1`;
  const query = {
    text: select,
    values: [keyword],
  };
  const {rows} = await pool.query(query);
  return rows.map((words) => words.keyword);
};

exports.getAccountByUsername = async (username) => {
  const select = `SELECT * FROM account WHERE username = $1`;
  const query = {
    text: select,
    values: [username],
  };
  const {rows} = await pool.query(query);
  return rows[0];
};

exports.getOwnListings = async (username) => {
  const select = `SELECT * FROM listing WHERE username = $1`;
  const query = {
    text: select,
    values: [username],
  };
  const {rows} = await pool.query(query);
  return rows;
};

//  -----------------------------------------------------------------
// will work on this tomorrow
exports.postResponse = async (response) => {
  const insert =
   `INSERT INTO listing_response(listing_id, username, msg, createtime)
    VALUES($1, $2, $3, $4);`;
  const date = new Date();
  const query = {
    text: insert,
    values: [response.listing_id,
      response.username,
      response.msg,
      date.toISOString()],
  };
  await pool.query(query);
};

exports.getResponsesByListingId = async (id) => {
  const select = `SELECT l.*, a.fname, a.lname
    FROM listing_response l
    JOIN account a 
    ON a.username = l.username
    WHERE l.listing_id = $1`;
  const query = {
    text: select,
    values: [id],
  };
  const {rows} = await pool.query(query);
  return rows;
};
