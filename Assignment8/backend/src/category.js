const db = require('./db');

/**
 * Function to return all elements in a category by path
 * @param {*} req
 * @param {*} res
 */
exports.getCategoryByPath = async (req, res) => {
  let path = req.query.path;
  //  console.log(path);
  // if there is no path, this will be the marketplace homepage
  if (path === null || path === undefined || path.length === 0) {
    path = '/marketplace';
  }
  const category = await db.getCategoryByPath(path);
  category.parts = await db.getCategoryParts(path);
  category.children = await db.getChildren(path);
  res.status(200).json(category);
};

// will implement later
exports.getSearchKeyWords = async (req, res) => {
  const keywords = await db.getKeywords(req.query.words);
  res.status(200).json(keywords);
};
