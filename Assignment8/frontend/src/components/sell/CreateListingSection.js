import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import {
  AppBar,
  Box,
  Button,
  Drawer,
  IconButton,
  Stack,
  TextField,
  Toolbar,
  Typography,
} from '@mui/material';
import React from 'react';
import {useHistory} from 'react-router-dom';
import * as ObjectUtils from '../../utils/ObjectUtils';

const drawerWidth = 350;

/**
 * Function to display the toolbar section
 * @param {*} props
 * @return {*} returns the toolbar section for create listing
 */
function ToolbarSection(props) {
  const history = useHistory();

  /**
   * Function to return to homepage
   * @param {*} event the home button click
   */
  function handleHomeButtonClick(event) {
    history.push('/');
  }

  return (
    <AppBar position="static" color="fbwhite">
      <Toolbar>
        <Typography variant="span" sx={{flexGrow: 1}} color="secondary">
          <IconButton onClick={handleHomeButtonClick}>
            <img
              src="2021_Facebook_icon.svg.png"
              style={{width: 40, display: 'inline'}}
              alt="Facebook"
            ></img>
          </IconButton>
        </Typography>
      </Toolbar>
    </AppBar>
  );
}

/**
 * Function to display the top section for the create listing section
 * @param {*} props onSaveClick
 * @return {*} the top section for the create listing section
 */
function TopSection(props) {
  const {onSaveClick} = props;

  return (
    <Stack direction="row" sx={{p: 1}}>
      <Box sx={{width: '65%'}}>
        <Box>Marketplace</Box>
        <Box sx={{fontSize: '25px', fontWeight: 'bold'}}>Item for Sale</Box>
      </Box>
      <Box
        sx={{
          display: 'flex',
          width: '35%',
          alignItems: 'flex-end',
          alignContent: 'flex-end',
        }}
      >
        <Button
          variant="contained"
          size="medium"
          sx={{textTransform: 'none'}}
          onClick={onSaveClick}
        >
          Save
        </Button>
      </Box>
    </Stack>
  );
}

/**
 * Function to display the account section
 * @param {*} props user
 * @return {*} returns the account section
 */
function AccountSection(props) {
  const {user} = props;
  const firstName = ObjectUtils.isNotEmpty(user) ? user.fname : '';
  const lastName = ObjectUtils.isNotEmpty(user) ? user.lname : '';

  return (
    <Stack direction="row" sx={{p: 1}}>
      <Box>
        <AccountCircleIcon fontSize="large" />
      </Box>
      <Box>
        <Box sx={{fontWeight: 'bold'}}>
          {firstName} {lastName}
        </Box>
        <Box>Listing to Marketplace</Box>
      </Box>
    </Stack>
  );
}

/**
 * Function to display the edit section
 * @param {*} props listing, onChange
 * @return {*} returns the edit section
 */
function EditSection(props) {
  const {listing, onChange} = props;

  return (
    <Stack spacing={1} sx={{p: 1}}>
      <TextField
        name="img"
        label="Add Photo"
        variant="outlined"
        value={listing.img}
        onChange={onChange}
      />
      <TextField
        name="title"
        label="Title"
        variant="outlined"
        value={listing.title}
        onChange={onChange}
      />
      <TextField
        name="price"
        label="Price"
        variant="outlined"
        value={listing.price}
        onChange={onChange}
      />
      <TextField
        name="catagory"
        label="Category"
        variant="outlined"
        value={listing.category}
        onChange={onChange}
      />
      <TextField
        name="condition"
        label="Condition"
        variant="outlined"
        value={listing.condition}
        onChange={onChange}
      />
      <TextField
        name="descript"
        label="Description"
        multiline
        rows={4}
        value={listing.desc}
        onChange={onChange}
      />
    </Stack>
  );
}

/**
 * Function to display the entire create listing section
 * @param {*} props
 * @return {*} returns the entire create listing section
 */
export default function CreateListingSection(props) {
  return (
    <Drawer
      variant="permanent"
      sx={{
        width: drawerWidth,
        flexShrink: 0,
        [`& .MuiDrawer-paper`]: {
          width: drawerWidth,
          boxSizing: 'border-box',
        },
      }}
    >
      <ToolbarSection {...props}></ToolbarSection>
      <TopSection {...props}></TopSection>
      <AccountSection {...props}></AccountSection>
      <EditSection {...props}></EditSection>
    </Drawer>
  );
}
