import {Box, Paper, Stack, Divider, CardMedia} from '@mui/material';
import {grey} from '@mui/material/colors';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import * as ObjectUtils from '../../utils/ObjectUtils';

/**
 * Function to get value of the string
 * @param {*} str the string to get
 * @param {*} defaultValue the string to return if str is empty
 * @return {*} the value of the string
 */
function getValue(str, defaultValue) {
  return ObjectUtils.ifStringEmpty(str, defaultValue);
}

/**
 * Function to get the background color
 * @param {*} str the string to get
 * @return {*} grey if string is empty, null if not
 */
function getColor(str) {
  return ObjectUtils.isStringEmpty(str) ? grey[400] : null;
}

/**
 * Function to get the value and color
 * @param {*} str the string to get
 * @param {*} defaultValue returns this value if str is empty
 * @return {*} value and color
 */
function getValues(str, defaultValue) {
  return [getValue(str, defaultValue), getColor(str)];
}

/**
 * Function to display the info section
 * @param {*} props listing, user
 * @return {*} returns the info section
 */
function InfoSection(props) {
  const {listing, user} = props;

  const [title, titleColor] = getValues(listing.title, 'Title');
  const [price, priceColor] = getValues(listing.price, 'Price');
  const [desc, descColor] = getValues(
    listing.descript,
    'Description will appear here.',
  );

  return (
    <Stack spacing={1}>
      <Box sx={{fontSize: '20px', fontWeight: 'bold', color: titleColor}}>
        {title}
      </Box>
      <Box sx={{color: priceColor}}>{price}</Box>
      <Box sx={{fontSize: '15px', fontWeight: 'bold', color: descColor}}>
        Details
      </Box>
      <Box sx={{color: descColor}}>{desc}</Box>
      <Divider />
      <Box sx={{fontSize: '15px', fontWeight: 'bold'}}>
        Seller Information
      </Box>
      <Stack direction="row" sx={{p: 1}}>
        <AccountCircleIcon fontSize="large" />
        <Box sx={{fontWeight: 'bold'}}>
          {user.fname} {user.lname}
        </Box>
      </Stack>
    </Stack>
  );
}

/**
 * Function to display the image section
 * @param {*} props img
 * @return {*} returns the image section
 */
function ImageSection(props) {
  const {img} = props.listing;
  const showImage = ObjectUtils.isStringNotEmpty(img);
  return (
    <Box>
      {!showImage && (
        <Box
          sx={{
            height: 800,
            p: 10,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            alignContent: 'center',
          }}
        >
          <Box sx={{fontSize: '15px', fontWeight: 'bold', color: grey[700]}}>
            Your Listing Preview
          </Box>
          <Box sx={{color: grey[700]}}>
            As you create your listings, you can preview how it will appear to
            others on Marketplace.
          </Box>
        </Box>
      )}
      {showImage && <CardMedia component="img" image={img}></CardMedia>}
    </Box>
  );
}

/**
 * Function to display the entire preview section
 * @param {*} props
 * @return {*} returns the entire preview section
 */
export default function PreviewSection(props) {
  return (
    <Box
      component="main"
      sx={{
        flexGrow: 1,
        p: 2,
        backgroundColor: grey[200],
      }}
      display={{xs: 'none', md: 'block'}}
    >
      <Paper sx={{p: 1}}>
        <h3>Preview</h3>
        <Paper elevation={0}>
          <Stack direction="row">
            <Box sx={{flexGrow: 1, backgroundColor: grey[200]}}>
              <ImageSection {...props}></ImageSection>
            </Box>
            <Box sx={{width: 350}}>
              <InfoSection {...props}></InfoSection>
            </Box>
          </Stack>
        </Paper>
      </Paper>
    </Box>
  );
}
