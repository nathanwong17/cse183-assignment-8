import {Box} from '@mui/material';
import React from 'react';
import {useHistory} from 'react-router-dom';
import * as ListingApi from '../../dummyapi/ListingApi';
import CreateListingSection from './CreateListingSection';
import PreviewSection from './PreviewSection';

/**
 * Function to display the entire sell page
 * @param {*} props
 * @return {*} returns the entire sell page
 */
export default function SellPage(props) {
  const {user} = props;
  const [listing, setListing] = React.useState({});
  const history = useHistory();

  /**
   * Function to handle data entry
   * @param {*} event data entry
   */
  function handleChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    const newListing = {...listing};
    newListing[name] = value;
    setListing(newListing);
  }

  /**
   * Function to handle save listing click
   * @param {*} event save listing click
   */
  function handleSaveClick(event) {
    listing.username = user.username;
    ListingApi.addListing(listing, user.accessToken).then((newListing) => {
      history.goBack();
    });
  }

  return (
    <Box sx={{display: 'flex'}}>
      <CreateListingSection
        {...props}
        listing={listing}
        onChange={handleChange}
        onSaveClick={handleSaveClick}
      ></CreateListingSection>
      <PreviewSection listing={listing} {...props}></PreviewSection>
    </Box>
  );
}
