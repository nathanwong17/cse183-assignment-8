import {blue, green, grey} from '@mui/material/colors';
import {createTheme, ThemeProvider} from '@mui/material/styles';
import React from 'react';
import {Route, Switch, useHistory} from 'react-router-dom';
import * as AccountApi from '../dummyapi/AccountApi';
import HomePage from './home/HomePage';
import YourListingPage from './listing/YourListingPage';
import CreateLoginPage from './login/CreateLoginPage';
import LoginPage from './login/LoginPage';
import SellPage from './sell/SellPage';

const theme = createTheme({
  palette: {
    fbblue: {
      main: '#036ffc',
      contrastText: '#ffffff',
    },
    fbwhite: {
      main: '#ffffff',
      contrastText: '#000000',
    },
    fblightblue: {
      main: blue[100],
    },
    fbgrey: {
      main: grey[500],
    },
    fbgreen: {
      main: green[500],
      contrastText: '#ffffff',
    },
  },
});

/**
 * Function to display the entire market place app
 * @param {*} props
 * @return {*} the entire marketplace app
 */
export default function MarketplaceApp(props) {
  //
  // This is the top level account user.
  // Once signed in, this should be populated.
  //
  const [user, setUser] = React.useState({});
  const history = useHistory();

  /**
   * Call API to login to the application.
   * This is called from the login page.
   * @param {*} login
   */
  function handleLogin(login) {
    AccountApi.login(login).then((newUser) => {
      setUser(newUser);
      if (history.location.pathname !== '/') {
        history.push('/');
      }
    });
  }

  /**
   * Function to handle new Login
   * @param {*} newLogin the new user
   */
  function handleSignUp(newLogin) {
    AccountApi.create(newLogin).then((newUser) => {
      setUser(newUser);
      history.push('/');
    });
  }

  //
  // Setup the routes for different pages.
  // For this to work, this component must
  // be wrapped in a BrowerRouter.
  //
  return (
    <ThemeProvider theme={theme}>
      <Switch>
        <Route path="/login">
          <LoginPage onLogin={handleLogin} />
        </Route>
        <Route path="/account">
          <CreateLoginPage onSignUp={handleSignUp} />
        </Route>
        <Route path="/yourlists">
          <YourListingPage user={user} />
        </Route>
        <Route path="/sell">
          <SellPage user={user} />
        </Route>
        <Route path="/">
          <HomePage user={user} onLogin={handleLogin} />
        </Route>
      </Switch>
    </ThemeProvider>
  );
}
