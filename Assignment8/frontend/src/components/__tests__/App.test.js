import {render, fireEvent} from '@testing-library/react';
import '@testing-library/jest-dom';
import {rest} from 'msw'
import {setupServer} from 'msw/node'
import App from '../../App';
import {screen, waitFor} from '@testing-library/react';
import {act} from 'react-dom/test-utils';

const URL = '/v0/marketplace'

const server = setupServer(
  rest.get(URL, (req, res, ctx) => {
    return res(ctx.json({message: 'Hello CSE183'}))
  }),
)

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

// Taken from common.js from Assignment 5
/**
 * @param {number} width
 */
 function setWidth(width) {
  global.innerWidth = width;
  act(() => {
    global.dispatchEvent(new Event('resize'));
  });
}

/**
 * works
 */
 test('Init App Render and see button and postings', async () => {
  render(<App />);
  //await waitFor(() => screen.getByText('Password'))
  await waitFor(() => screen.getByText('Learn more'))
  await waitFor(() => screen.getByText('$250'))
  await waitFor(() => screen.getByText('$599'))
  await waitFor(() => screen.getByText('81 miles'))
});

// /**
//  */
//  test('Go to start of login page', async () => {
//   render(<App />);
//   setWidth(550);
//   //await waitFor(() => screen.getByText('Password'))
//   fireEvent.click(screen.getByTestId ('topbarbutton'));
//   await waitFor(() => screen.getByText('Create new account'))
//   await waitFor(() => screen.getByText('Log into Facebook'))
// });

/**
 * works
 */
test('Login Page - Login', async () => {
  render(<App />);
  // const input = utils.getByLabelText()
  setWidth(550);
  fireEvent.click(screen.getByTestId ('topbarbutton'));
  fireEvent.change(screen.getByLabelText('Email or phone number'), {target: {value: 'nwong@nothing.com'}});
  fireEvent.change(screen.getByLabelText('Password'), {target: {value: 'hello123'}});
  fireEvent.click(screen.getByTestId ('LoginButton'));
  await waitFor(() => screen.getByText('Nathan Wong'))
});

/**
 * works
 */
 test('Login Page - Create New User', async () => {
  render(<App />);
  // const input = utils.getByLabelText()
  setWidth(550);
  const date = new Date();
  const t = date.getTime();
  fireEvent.click(screen.getByTestId ('topbarbutton'));
  fireEvent.click(screen.getByTestId ('CreateNewAccount'));
  fireEvent.change(screen.getByLabelText('First name'), {target: {value: 'nathan'}});
  fireEvent.change(screen.getByLabelText('Last name'), {target: {value: 'wong'}});
  fireEvent.change(screen.getByLabelText('Email or phone number'), {target: {value: `${t}@testing.com`}});
  fireEvent.change(screen.getByLabelText('New password'), {target: {value: 'cse183pass'}});
  fireEvent.click(screen.getByTestId ('SignUp'));
  await waitFor(() => screen.getByText('nathan wong'));
});

// works
test('Category List - Click on one category', async () => {
  render(<App />);
  setWidth(550);
  fireEvent.click(screen.getByTestId('All Categories'));
  await waitFor(() => screen.getByText('Boats'));
  fireEvent.click(screen.getByLabelText('Boats'));
  await waitFor(() => screen.getByText('Cruise Ships'));
  fireEvent.click(screen.getByTestId('/marketplace/boats/cruiseships'));
  fireEvent.click(screen.getByTestId('TopCategoryButton'));
  await waitFor(() => screen.getByText('Cars'));
  fireEvent.click(screen.getByLabelText('Cars'));
  await waitFor(() => screen.getByText('Coupes'));
  fireEvent.click(screen.getByTestId('/marketplace/cars/coupes'));
  await waitFor(() => screen.getByText('Cars'));
  fireEvent.click(screen.getByTestId('br/marketplace/cars'));
  await waitFor(() => screen.getByText('Coupes'));
  fireEvent.click(screen.getByTestId('br/marketplace'));
  await waitFor(() => screen.getByText('All Categories'));
});


test('Category Select Dialogue - Close Click', async () => {
  render(<App />);
  setWidth(550);
  await waitFor(() => screen.getByText('All Categories'));
  fireEvent.click(screen.getByTestId('All Categories'));
  await waitFor(() => screen.getByText('Boats'));
  fireEvent.click(screen.getByTestId('closeCategorySelectDialog'));
  await waitFor(() => screen.getByText('All Categories'));
});

test('Listing - Select Listing', async () => {
  render(<App />);
  setWidth(550);
  await waitFor(() => screen.getByText('All Categories'));
  await waitFor(() => screen.getByText('Computer Desk, San Jose East, CA'));
  fireEvent.click(screen.getByTestId('card16'));
  // await waitFor(() => screen.getByText('Listed about an hour ago in San Jose East, CA'));
  await waitFor(() => screen.getByTestId('closeViewListing'));
  fireEvent.click(screen.getByTestId('closeViewListing'));
  await waitFor(() => screen.getByText('All Categories'));
});

// test('Search Bar - Search', async () => {
//   render(<App />);
//   setWidth(550);
//   await waitFor(() => screen.getByText('All Categories'));
//   screen.getByLabelText('Search Marketplace').focus();
//   fireEvent.change(screen.getByLabelText('Search Marketplace'), {target: {value: 'ford'}});
//   await waitFor(() => screen.getByText('Ford'));
//   fireEvent.click(screen.getByTestId('searchItemButton'));
//   await waitFor(() => screen.getByText('All Categories'));
// })

test('View Listing Page - View Your Listings', async () => {
  render(<App />);
  setWidth(550);
  fireEvent.click(screen.getByTestId ('topbarbutton'));
  fireEvent.change(screen.getByLabelText('Email or phone number'), {target: {value: 'nwong@nothing.com'}});
  fireEvent.change(screen.getByLabelText('Password'), {target: {value: 'hello123'}});
  fireEvent.click(screen.getByTestId ('LoginButton'));
  await waitFor(() => screen.getByText('Nathan Wong'));
  fireEvent.click(screen.getByTestId ('viewOwnListings'));
  await waitFor(() => screen.getByText('Your listings'));
  await waitFor(() => screen.getByTestId('card3'));
  fireEvent.click(screen.getByTestId('card3'));
  await waitFor(() => screen.getByTestId('closeViewListing'));
  fireEvent.click(screen.getByTestId('closeViewListing'));
})
// test('Category Section - Chip Click', async () => {
//   render(<App />);
//   setWidth(550);
//   fireEvent.click(screen.getByTestId('All Categories'));
//   await waitFor(() => screen.getByText('Boats'))
//   fireEvent.click(screen.getByLabelText('Boats'));
//   await waitFor(() => screen.getByText('Cruise Ships'))
//   fireEvent.click(screen.getByTestId('/marketplace/boats/cruiseships'));
// })

/**
 * works
 */
 test('Login Page - to sell page', async () => {
  render(<App />);
  // const input = utils.getByLabelText()
  setWidth(550);
  fireEvent.click(screen.getByTestId ('topbarbutton'));
  fireEvent.change(screen.getByLabelText('Email or phone number'), {target: {value: 'nwong@nothing.com'}});
  fireEvent.change(screen.getByLabelText('Password'), {target: {value: 'hello123'}});
  fireEvent.click(screen.getByTestId ('LoginButton'));
  await waitFor(() => screen.getByText('Nathan Wong'))
  fireEvent.click(screen.getByTestId  ('Sell'));
  await waitFor(() => screen.getByText('Item for Sale'))
});

// /**
//  * works
//  */
//  test('Login Page - to sell page', async () => {
//   render(<App />);
//   // const input = utils.getByLabelText()
//   setWidth(550);
//   fireEvent.click(screen.getByTestId ('topbarbutton'));
//   fireEvent.change(screen.getByLabelText('Email or phone number'), {target: {value: 'nwong@nothing.com'}});
//   fireEvent.change(screen.getByLabelText('Password'), {target: {value: 'hello123'}});
//   fireEvent.click(screen.getByTestId ('LoginButton'));
//   await waitFor(() => screen.getByText('Nathan Wong'))
//   fireEvent.click(screen.getByTestId  ('You'));
//   await waitFor(() => screen.getByText('Your listings'))
// });