import {Drawer, Toolbar, Box, Typography} from '@mui/material';
import SearchSection from './SearchSection';
import Filters from './Filters';
// import CategorySection from './CategorySection';
import CategoryList from './CategoryList';

const drawerWidth = 350;

/**
 * Function to create the nav bar on the side
 * @param {*} props
 * @return {*} the navigation section on the side
 */
export default function NavigationSection(props) {
  return (
    <Box display={{xs: 'none', md: 'block'}}>
      <Drawer
        variant='permanent'
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          [`& .MuiDrawer-paper`]: {
            width: drawerWidth,
            boxSizing: 'border-box',
          },
        }}
      >
        <Toolbar />
        <Box sx={{overflow: 'auto'}}>
          <Typography
            sx={{
              fontSize: 'large',
              fontWeight: 'bold'}}>
              Marketplace
          </Typography>
          <SearchSection></SearchSection>
        </Box>
        <Box>
          <Filters></Filters>
          <CategoryList></CategoryList>
        </Box>
      </Drawer>
    </Box>
  );
}
