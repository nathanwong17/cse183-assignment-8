import {Stack} from '@mui/material';
import SearchSection from './SearchSection.js';
import ActionSection from './ActionSection';
import CategorySection from './CategorySection';
import * as ObjectUtils from '../../utils/ObjectUtils';

/**
 * Function to return the mobile version of selection section
 * @param {*} props
 * @return {*} the mobile selection section
 */
export default function MobileSelectionSection(props) {
  const {category, onCategoryClick, onCategorySelect, user, onSearch} = props;
  const showAction =
    ObjectUtils.isEmpty(category) || category.parts.length === 1;
  const showCategory =
    ObjectUtils.isNotEmpty(category) && category.parts.length > 1;

  return (
    <Stack display={{xs: 'block', md: 'none'}} spacing={1} sx={{p: 1}}>
      {showAction && (
        <ActionSection
          onCategoryClick={onCategoryClick}
          user={user}
        ></ActionSection>
      )}
      {showCategory && (
        <CategorySection
          category={category}
          onSelect={onCategorySelect}
          onCategoryClick={onCategoryClick}
        ></CategorySection>
      )}
      <SearchSection onSearch={onSearch}></SearchSection>
    </Stack>
  );
}
