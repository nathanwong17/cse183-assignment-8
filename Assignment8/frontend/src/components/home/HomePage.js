import {Box, Dialog, Slide} from '@mui/material';
import TopBar from './TopBar';
import NavigationSection from './NavigationSection';
import TopSection from './TopSection';
import {grey} from '@mui/material/colors';
import MobileSelectionSection from './MobileSelectionSection';
import ListingSection from './ListingSelection';
import React from 'react';
import * as CategoryApi from '../../dummyapi/CategoryApi';
import CategorySelectDialog from './CategorySelectDialogue';
import * as ObjectUtils from '../../utils/ObjectUtils';
import * as ListingApi from '../../dummyapi/ListingApi';
import ViewListingPage from '../listing/ViewListingPage';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

/**
 * Function to show the view listing dialog
 * @param {*} props
 * @return {*} returns the dialog for viewing listing
 */
function ViewListingDialog(props) {
  const {user, open, onCloseClick, listing} = props;
  return (
    <Dialog fullScreen open={open} TransitionComponent={Transition}>
      <ViewListingPage
        user={user}
        onCloseClick={onCloseClick}
        listing={listing}
      ></ViewListingPage>
    </Dialog>
  );
}

/**
 * Function to display home page.
 * Put this in it's own component to make putting things in App.js clearer
 *
 * @param {*} props
 * @return {*} the hopepage with all components
 */
export default function HomePage(props) {
  const {user, onLogin} = props;

  const [viewListingOpen, setViewListingOpen] = React.useState(false);
  const [currentListing, setCurrentListing] = React.useState({});
  const [mareketPlaceCategory, setMarketPlaceCategory] = React.useState(null);
  const [currentCategory, setCurrentCategory] = React.useState(null);
  const [categorySelectOpen, setCategorySelectOpen] = React.useState(false);
  const [listings, setListings] = React.useState([]);
  const currentCategoryChildren =
    ObjectUtils.isNotEmpty(mareketPlaceCategory) ?
      mareketPlaceCategory.children : [];

  //
  // initialize init categories
  //
  React.useEffect(() => {
    if (mareketPlaceCategory === null) {
      CategoryApi.getCategory(null)
        .then((newCategory) => {
          console.log(newCategory);
          setMarketPlaceCategory(newCategory);
          setCurrentCategory(newCategory);
        });
    }
  }, [mareketPlaceCategory, currentCategory]);

  React.useEffect(() => {
    if (currentCategory !== null) {
      ListingApi.getListings(currentCategory.cat_path)
        .then((newListings) => {
          setListings(newListings);
        });
    }
  }, [currentCategory]);

  /**
   * Function to handle category selection
   * @param {*} category
   */
  function handleCategorySelect(category) {
    setCategorySelectOpen(false);
    if (category !== undefined && category !== null) {
      CategoryApi.getCategory(category).then((newCategory) => {
        setCurrentCategory(newCategory);
      });
    }
  }

  /**
   * Function to handle closing the view listing dialog
   * @param {*} event
   */
  function handleViewListingClose(event) {
    setViewListingOpen(!viewListingOpen);
  }

  /**
   * Function to handle a listing click
   * @param {*} listing the selected listing
   */
  function handleListingClick(listing) {
    console.log('Listing', listing);
    setCurrentListing(listing);
    setViewListingOpen(!viewListingOpen);
  }

  /**
   * Function to handle the search bar
   * @param {*} keywords the keywords in the search bar
   */
  function handleSearch(keywords) {
    console.log('handle search', keywords);
    ListingApi.getListingBySearch(keywords).then((newListings) => {
      console.log('handle search', newListings);
      setListings(newListings);
    });
  }

  return (
    <Box sx={{display: 'flex'}}>
      <TopBar onLogin={onLogin} user={user}></TopBar>
      <NavigationSection></NavigationSection>
      <Box
        component='main'
        sx={{
          flexGrow: 1,
          p: {sx: 0, md: 1},
          backgroundColor: {sx: '#ffffff', md: grey[200]},
        }}
      >
        <Box sx={{height: 40}}></Box>
        <TopSection></TopSection>
        <MobileSelectionSection
          category={currentCategory}
          onCategoryClick={(event) => setCategorySelectOpen(true)}
          onCategorySelect={handleCategorySelect}
          user={user}
          onSearch={handleSearch}
        ></MobileSelectionSection>
        <ListingSection
          listings={listings}
          onListingClick={handleListingClick}
        ></ListingSection>
        <CategorySelectDialog
          open={categorySelectOpen}
          onSelect={handleCategorySelect}
          categories={currentCategoryChildren}
        ></CategorySelectDialog>
      </Box>
      <ViewListingDialog
        open={viewListingOpen}
        user={user}
        onCloseClick={handleViewListingClose}
        listing={currentListing}
      ></ViewListingDialog>
    </Box>
  );
}
