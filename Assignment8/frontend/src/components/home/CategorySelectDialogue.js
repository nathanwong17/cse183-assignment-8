import React from 'react';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import Box from '@mui/material/Box';
import CategoryList from './CategoryList';

/**
 * Function to display the category select dialog
 * @param {*} props
 * @return {*} returns the pop up box for the category select
 */
export default function CategorySelectDialog(props) {
  const {onSelect, categories, open} = props;

  /**
   * Function to handle close click
   * @param {*} event
   */
  function handleClose(event) {
    if (onSelect !== undefined && onSelect !== null) {
      onSelect(null);
    }
  }

  return (
    <Dialog open={open}>
      <DialogTitle>
        <Box sx={{display: 'flex', fontWeight: 'bold', alignItems: 'center'}}>
          Select Category
        </Box>
        <IconButton
          aria-label='close'
          onClick={handleClose}
          data-testid='closeCategorySelectDialog'
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent dividers>
        <CategoryList
          categories={categories}
          onSelect={onSelect}
        ></CategoryList>
      </DialogContent>
    </Dialog>
  );
}
