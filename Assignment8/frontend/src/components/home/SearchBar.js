import PageviewTwoToneIcon from '@mui/icons-material/PageviewTwoTone';
import SearchIcon from '@mui/icons-material/Search';
import ClickAwayListener from '@mui/material/ClickAwayListener';
import {grey} from '@mui/material/colors';
import IconButton from '@mui/material/IconButton';
import InputBase from '@mui/material/InputBase';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Paper from '@mui/material/Paper';
import Popper from '@mui/material/Popper';
import React from 'react';
import {Box} from '@mui/system';

/**
 * Function to display a functional search bar
 * @param {*} props
 * @return {*} the search bar
 */
export default function SearchBar(props) {
  const [value, setValue] = React.useState('');
  const [anchorEl, setAnchorEl] = React.useState(null);

  const open = Boolean(anchorEl);
  const {values, onSearch, onChange} = props;

  const list = values.map((s) => {
    return (
      <ListItem disablePadding>
        <ListItemButton onClick={(event) => handleListClick(s)}>
          <ListItemIcon>
            <PageviewTwoToneIcon />
          </ListItemIcon>
          <ListItemText primary={s} />
        </ListItemButton>
      </ListItem>
    );
  });

  /**
   * Function to handle click of List item
   * @param {*} newValue the value of the new search item
   */
  function handleListClick(newValue) {
    setValue(newValue);
    setAnchorEl(null);
    if (onSearch !== undefined && onSearch !== null) {
      onSearch(newValue);
    }
  }

  /**
   * Function to handle the focus
   * @param {*} event
   */
  function handleFocus(event) {
    setAnchorEl(event.currentTarget);
  }

  /**
   * Function to handle the change of new selection
   * @param {*} event
   */
  function handleChange(event) {
    setValue(event.target.value);
    // setAnchorEl(event.currentTarget);
    if (onChange !== undefined && onChange !== null) {
      onChange(event.target.value);
    }
  }

  /**
   * Function to handle the search
   * @param {*} event
   */
  function handleSearch(event) {
    setAnchorEl(null);
    if (onSearch !== undefined && onSearch !== null) {
      onSearch(value);
    }
  }

  /**
   * Function to handle closing of search bar
   * @param {*} event
   */
  function handleClose(event) {
    setAnchorEl(null);
  }

  return (
    <ClickAwayListener onClickAway={handleClose}>
      <Box>
        <Paper
          component='form'
          sx={{
            p: '2px 2px',
            display: 'flex',
            alignItems: 'center',
            backgroundColor: grey[200],
          }}
          elevation={0}
        >
          <IconButton
            sx={{p: '5px'}}
            aria-label='search'
            onClick={handleSearch}
            data-testid='searchItemButton'
          >
            <SearchIcon />
          </IconButton>

          <InputBase
            sx={{ml: 1, flex: 1}}
            placeholder='Search Marketplace'
            value={value}
            onChange={handleChange}
            onFocus={handleFocus}
            data-testid='searchField'
          />
        </Paper>
        <Popper open={open} anchorEl={anchorEl} placement='bottom-start'>
          <Box sx={{backgroundColor: '#ffffff', width: 300}}>
            <List>{list}</List>
          </Box>
        </Popper>
      </Box>
    </ClickAwayListener>
  );
}
