import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import TuneIcon from '@mui/icons-material/Tune';
// will need to create pop up window for advanced selection of filters

/**
 * Function to show filter component
 * @return {*} the filter section
 */
export default function Filters() {
  return (
    <div>
      <Stack direction="row" spacing={2}>
        <Button
          startIcon={<LocationOnIcon />}>
            Santa Cruz - 40 mi
        </Button>
        <Button startIcon={<TuneIcon />}>Filters</Button>
      </Stack>
    </div>
  );
}
