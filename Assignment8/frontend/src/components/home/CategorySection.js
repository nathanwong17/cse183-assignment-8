import {Box, Link, Breadcrumbs, Button, Stack, Chip} from '@mui/material';
import ExpandCircleDownRoundedIcon from
'@mui/icons-material/ExpandCircleDownRounded';
import * as ObjectUtils from '../../utils/ObjectUtils';

// maybe need to pass in properties to this component
// to update state

/**
 * Function to create the category section
 * @param {*} props
 * @return {*} the category section
 */
export default function CategorySection(props) {
  const {category, onSelect, onCategoryClick} = props;
  const topPath = category.parts[category.parts.length - 1];

  /**
   * Function to handle click of breadcrumbs
   * @param {*} event
   * @param {*} newCategory the new category to go to
   */
  function handleBreadCrumbClick(event, newCategory) {
    event.preventDefault();
    if (ObjectUtils.isNotEmpty(onSelect)) {
      onSelect(newCategory);
    }
  }

  /**
   * Function to handle click of chips
   * @param {*} event
   * @param {*} newCategory the new category to go to after chip click
   */
  function handleChipClick(event, newCategory) {
    if (ObjectUtils.isNotEmpty(onSelect)) {
      onSelect(newCategory);
    }
  }

  /**
   * Function to handle category button click
   * @param {*} event
   */
  function handleButtonClick(event) {
    if (ObjectUtils.isNotEmpty(onCategoryClick)) {
      onCategoryClick(event);
    }
  }

  const breadcrumbs = category.parts.map((path) => (
    <Link
      underline='hover'
      key={path.cat_path}
      data-testid={'br' + path.cat_path}
      color='inherit'
      href='/'
      onClick={(event) => handleBreadCrumbClick(event, path)}
    >
      {path.cat_name}
    </Link>
  ));

  const subCategories = category.children.map((subcat) => (
    <Chip
      label={subcat.cat_name}
      key={subcat.cat_path}
      data-testid={subcat.cat_path}
      onClick={(event) => handleChipClick(event, subcat)}
    ></Chip>
  ));

  // smaller arrow bread crumbs
  return (
    <Box>
      <Breadcrumbs separator='›' aria-label='breadcrumb'>
        {breadcrumbs}
      </Breadcrumbs>
      <Button
        variant='text'
        endIcon={<ExpandCircleDownRoundedIcon />}
        data-testid='TopCategoryButton'
        sx={{
          textTransform: 'none',
          color: '#000000',
          fontSize: 'large',
          fontWeight: 'bold',
        }}
        onClick={handleButtonClick}
      >
        {topPath.cat_name}
      </Button>
      <Stack direction='row' spacing={1} sx={{flexWrap: 'wrap'}}>
        {subCategories}
      </Stack>
    </Box>
  );
}

