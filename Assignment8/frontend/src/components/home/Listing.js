import {
  Grid,
  Card,
  CardMedia,
  CardContent,
  Typography,
  CardActionArea,
} from '@mui/material';

import * as ObjectUtils from '../../utils/ObjectUtils';

/**
 * Function to show listing items as cards
 * @param {*} props the listed items along with their properties
 * @return {*} the formatted listings
 */
export default function Listing(props) {
  const {listing, onListingClick} = props;

  /**
   * Function to handle the click of a listing
   * @param {*} event the listing click
   */
  function handleClick(event) {
    if (ObjectUtils.isNotEmpty(onListingClick)) {
      onListingClick(listing);
    }
  }

  return (
    <Grid item xs={6} sm={4} lg={3} xl={2}>
      <Card
        elevation={0}
        onClick={handleClick}
        data-testid={'card' + listing.id}>
        <CardActionArea>
          <CardMedia
            component='img'
            height='250'
            image={listing.img}
            alt={listing.desc}
          />
          <CardContent>
            <Typography
              gutterBottom
              component='div'
              sx={{fontWeight: 'bold'}}
            >
            ${listing.price}
            </Typography>
            <Typography component='div'>{listing.desc}</Typography>
            <Typography variant='body2' color='textSecondary' component='p'>
              {listing.descript}, {listing.city}, {listing.stat}
            </Typography>
            <Typography variant='body2' color='textSecondary' component='p'>
              {listing.milage}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </Grid>
  );
}
