import {
  AppBar,
  Box,
  Button,
  TextField,
  Toolbar,
  Typography,
} from '@mui/material';
import React from 'react';
import {useHistory} from 'react-router-dom';
import * as ObjectUtils from '../../utils/ObjectUtils';

//
// Credits:
// Logo from https://logos-world.net/facebook-logo/
// Hide box https://stackoverflow.com/questions/58189940/material-ui-grid-does-not-hide-when-using-display
//

/**
 * Function to create the top bar
 * @param {*} props
 * @return {*} the top bar
 */
export default function TopBar(props) {
  const {user, onLogin} = props;
  const isSignedIn = ObjectUtils.isPropNotEmpty(user, 'accessToken');

  const [login, setLogin] = React.useState({});
  const history = useHistory();

  /**
   * Function to handle change in username and password
   * @param {*} event
   */
  function handleChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    const nu = {...login};
    nu[name] = value;
    setLogin(nu);
  }

  /**
   * Function to handle login button click
   * @param {*} event the login click
   */
  function handleLoginClick(event) {
    if (
      ObjectUtils.isStringEmpty(login.username) ||
      ObjectUtils.isStringEmpty(login.password)
    ) {
      history.push('/login');
    } else if (ObjectUtils.isNotEmpty(onLogin)) {
      onLogin(login);
    }
  }

  return (
    <AppBar
      position='fixed'
      color='fbwhite'
      sx={{zIndex: (theme) => theme.zIndex.drawer + 1}}>
      <Toolbar>
        <Typography variant='span' sx={{flexGrow: 1}} color='secondary'>
          <img
            src='Facebook-Logo.png'
            style={{width: 110, display: 'inline'}}
            alt='Facebook'
          ></img>
        </Typography>

        {!isSignedIn && (
          <Box display={{xs: 'none', md: 'inline'}}>
            <TextField
              name='username'
              label='Email or Phone'
              type='text'
              variant='outlined'
              size='small'
              sx={{marginRight: 1}}
              onChange={handleChange}
            />

            <TextField
              name='password'
              label='Password'
              type='password'
              autoComplete='current-password'
              variant='outlined'
              size='small'
              sx={{marginRight: 1}}
              onChange={handleChange}
            />
          </Box>
        )}

        {!isSignedIn && (
          <Button
            variant='contained'
            color='fbblue'
            size='large'
            sx={{textTransform: 'none'}}
            onClick={handleLoginClick}
            data-testid='topbarbutton'
          >
            Log In
          </Button>
        )}

        {isSignedIn && (
          <Box display="inline" data-testid='TopBarUsername'>
            {user.fname} {user.lname}
          </Box>
        )}
      </Toolbar>
    </AppBar>
  );
}
