import React from 'react';
import SearchBar from './SearchBar';
import * as CategoryApi from '../../dummyapi/CategoryApi';
import * as ObjectUtils from '../../utils/ObjectUtils';

/**
 * Function to display the search section
 * @param {*} props
 * @return {*} the search section
 */
export default function SearchSection(props) {
  const {onSearch} = props;
  const [values, setValues] = React.useState([]);

  /**
   * Function to handle search bar searches
   * @param {*} search the search bar contents
   */
  function handleChange(search) {
    console.log('search', search);
    if (ObjectUtils.isNotEmpty(search) && search.length > 0) {
      CategoryApi.getSearchPhrases(search).then((phrases) => {
        setValues(phrases);
      });
    } else {
      setValues([]);
    }
  }

  return (
    <SearchBar
      values={values}
      onChange={handleChange}
      onSearch={onSearch}
    ></SearchBar>
  );
}
