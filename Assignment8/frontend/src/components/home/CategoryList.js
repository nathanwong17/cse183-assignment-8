import {
  ListItem,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from '@mui/material';
import PageviewTwoToneIcon from '@mui/icons-material/PageviewTwoTone';

import * as ObjectUtils from '../../utils/ObjectUtils';

/**
 * Function to display the popup category select
 * @param {*} props
 * @return {*} the category select list
 */
export default function CategoryList(props) {
  const {categories, onSelect} = props;
  const categoryList = ObjectUtils.isNotEmpty(categories) ? categories : [];

  /**
   * Function to handle list click
   * @param {*} category
   */
  function handleListClick(category) {
    if (onSelect !== undefined && onSelect !== null) {
      onSelect(category);
    }
  }

  const list = categoryList.map((category) => {
    return (
      <ListItem disablePadding
        key={category.cat_path}>
        <ListItemButton
          onClick={(event) => handleListClick(category)}
          aria-label={category.cat_name}
          data-testid={'cl' + category.cat_path}>
          <ListItemIcon>
            <PageviewTwoToneIcon />
          </ListItemIcon>
          <ListItemText primary={category.cat_name} />
        </ListItemButton>
      </ListItem>
    );
  });
  return <List>{list}</List>;
}
