import {AccountCircle} from '@mui/icons-material';
import {Chip, Stack} from '@mui/material';
import {useHistory} from 'react-router-dom';
import * as AccountUtils from '../../utils/AccountUtils';

/**
 * Function to display the action section with chips
 * @param {*} props
 * @return {*} the action section with chips
 */
export default function ActionSection(props) {
  const {onCategoryClick, user} = props;
  const history = useHistory();
  const isSignedIn = AccountUtils.isSignedIn(user);

  /**
   * Function to handle your list click
   * @param {*} event
   */
  function handleYourListClick(event) {
    history.push('/yourlists');
  }

  /**
   * Function to handle sell click
   * @param {*} event
   */
  function handleSellClick(event) {
    history.push('/sell');
  }

  return (
    <Stack direction="row" spacing={1}>
      <Chip
        icon={<AccountCircle></AccountCircle>}
        label="You"
        size="large"
        onClick={handleYourListClick}
        disabled={!isSignedIn}
        data-testid='viewOwnListings'
      />
      <Chip
        label="Sell"
        size="large"
        onClick={handleSellClick}
        disabled={!isSignedIn}
        data-testid='Sell'
      />
      <Chip
        label="All Categories"
        size="large"
        onClick={onCategoryClick}
        data-testid='All Categories' />
    </Stack>
  );
}
