import Grid from '@mui/material/Grid';
import Listing from './Listing';
import * as ObjectUtils from '../../utils/ObjectUtils';

/**
 * Function to show all listing items in the app. will replace for json file
 * @param {*} props will be defined later
 * @return {*} the listing selection
 */
export default function ListingSelection(props) {
  const {listings, onListingClick} = props;
  const listingCheck = ObjectUtils.isNotEmpty(listings) ? listings : [];
  const list = listingCheck.map((listing) => (
    <Listing
      key={listing.id}
      listing={listing}
      onListingClick={onListingClick}
    ></Listing>
  ));
  return (
    <Grid container spacing={1}>
      {list}
    </Grid>
  );
}
