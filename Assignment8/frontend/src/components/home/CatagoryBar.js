//  import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import React from 'react';
import CategorySection from './CategorySection';
import MarketPlaceSearchBar from './SearchBar';
//  import TextField from '@material-ui/core/TextField';
//  import {makeStyles} from '@material-ui/core/styles';
import Divider from '@mui/material/Divider';

// const stylesToUse = makeStyles ((theme) => ({
//     textField: {
//       marginLeft: theme.spacing(1),
//       marginRight: theme.spacing(1),
//       width: '25ch',
//     },
// }));

/**
 * Simple component with no state.
 *
 * @return {object} JSX
 */
export default function Catagorybar() {
    //  const clas = stylesToUse();
    return (
        <div>
        <CssBaseline />
            <Divider/>
            <CategorySection></CategorySection>
            {/**
             *
             * <TextField
               * label='Search Marketplace'
                *style={{margin: 8, width: '85%'}}
                *variant="filled"
            />*/}
            <MarketPlaceSearchBar></MarketPlaceSearchBar>
            <Divider/>
        </div>
    );
}
