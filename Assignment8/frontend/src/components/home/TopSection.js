import {Stack, Box, Paper, Button} from '@mui/material';

/**
 * Function to display the top section
 * @param {*} props
 * @return {*} the top section with improvements
 */
export default function TopSection(props) {
  return (
    <Paper elevation={0} sx={{backgroundColor: '#ffbab0'}}>
      <Stack direction='row' sx={{p: 1}}>
        <Box sx={{flexGrow: 1, p: 1}}>
          <h3>
            Buy and sell items locally or have something new shipped from
            stores.
          </h3>
          <p>Log in to get the full Facebook Marketplace experience.</p>
          <Stack direction='row' spacing={1}>
            <Button
              variant='contained'
              color='secondary'
              sx={{
                textTransform: 'none',
                backgroundColor: '#ffffff',
                color: '#000000',
              }}
            >
              Log In
            </Button>
            <Button
              variant='contained'
              color='secondary'
              sx={{
                flexGrow: 1,
                textTransform: 'none',
                backgroundColor: '#ffffff',
                color: '#000000',
              }}
            >
              Learn more
            </Button>
          </Stack>
        </Box>
        <Box display={{xs: 'none', md: 'block'}}>
          <img
            src='listing_images/facebook-top-section-001.png'
            alt='facebook'
            style={{height: '200px'}}
          ></img>
        </Box>
      </Stack>
    </Paper>
  );
}
