import SendIcon from '@mui/icons-material/Send';
import {
  AppBar,
  Box,
  Button,
  Dialog,
  IconButton,
  Slide,
  Stack,
  Toolbar,
  Typography,
} from '@mui/material';
import {grey} from '@mui/material/colors';
import React from 'react';
import {useHistory} from 'react-router-dom';
import * as ListingApi from '../../dummyapi/ListingApi';
import ListingSection from '../home/ListingSelection';
import ViewListingPage from '../listing/ViewListingPage';

/**
 * Function to display the entire toolbar section
 * @param {*} props
 * @return {*} the toolbar section
 */
function ToolbarSection(props) {
  const history = useHistory();

  /**
   * Function to handle home button click
   * @param {*} event
   */
  function handleHomeButtonClick(event) {
    history.push('/');
  }

  return (
    <AppBar position="fixed" color="fbwhite">
      <Toolbar>
        <Typography variant="span" sx={{flexGrow: 1}} color="secondary">
          <IconButton onClick={handleHomeButtonClick}>
            <img
              src="2021_Facebook_icon.svg.png"
              style={{width: 40, display: 'inline'}}
              alt="Facebook"
            ></img>
          </IconButton>
        </Typography>
      </Toolbar>
    </AppBar>
  );
}

/**
 * Function to display the top section
 * @param {*} props onAddClick
 * @return {*} the top section
 */
function TopSection(props) {
  const {onAddClick} = props;
  return (
    <Stack spacing={2} p={2}>
      <Box sx={{fontSize: 'small', color: grey[700]}}>Marketplace</Box>
      <Box sx={{fontWeight: 'bold', fontSize: '25px'}}>Your listings</Box>
      <Button
        variant="contained"
        color="fblightblue"
        sx={{textTransform: 'none'}}
        onClick={onAddClick}
      >
        + Create new listing
      </Button>
    </Stack>
  );
}

/**
 * Function to display section when user has no listings
 * @param {*} props
 * @return {*} returns this section if no listings
 */
function NoListingSection(props) {
  return (
    <Stack
      spacing={2}
      p={2}
      sx={{alignContent: 'center', alignItems: 'center'}}
    >
      <SendIcon color="fbgrey" fontSize="large" />
      <Box sx={{color: grey[500], fontSize: 'large'}}>
        When you start selling, your listings will appear here
      </Box>
    </Stack>
  );
}

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

/**
 * Function to display the view listing dialog
 * @param {*} props user, open, onCloseClick, listing
 * @return {*} returns the view listing dialog
 */
function ViewListingDialog(props) {
  const {user, open, onCloseClick, listing} = props;
  return (
    <Dialog fullScreen open={open} TransitionComponent={Transition}>
      <ViewListingPage
        user={user}
        onCloseClick={onCloseClick}
        listing={listing}
      ></ViewListingPage>
    </Dialog>
  );
}

/**
 * Function to display the entire user listing page
 * @param {*} props user
 * @return {*} returns the user listing page
 */
export default function YourListingPage(props) {
  const {user} = props;
  const [listings, setListings] = React.useState([]);
  const [currentListing, setCurrentListing] = React.useState({});
  const [viewListingOpen, setViewListingOpen] = React.useState(false);
  const showListings = listings.length > 0;
  const history = useHistory();

  React.useEffect(() => {
    console.log('your listing initialize in useEffect');
    if (listings.length === 0) {
      ListingApi.getListingByUser(user.username, user.accessToken)
        .then((newListings) => {
          console.log('api call done');
          setListings(newListings);
        });
    }
  });

  /**
   * Function to handle add new listing click
   * @param {*} event the add new listing click
   */
  function handleAddClick(event) {
    history.push('/sell');
  }

  /**
   * Function to handle view listing close
   * @param {*} event close view listing
   */
  function handleViewListingClose(event) {
    setViewListingOpen(!viewListingOpen);
  }

  /**
   * Function to handle listing click
   * @param {*} listing the clicked listing
   */
  function handleListingClick(listing) {
    setCurrentListing(listing);
    setViewListingOpen(!viewListingOpen);
  }

  return (
    <>
      <ToolbarSection></ToolbarSection>
      <Box sx={{height: 50}}></Box>
      <TopSection onAddClick={handleAddClick}></TopSection>
      {!showListings && <NoListingSection></NoListingSection>}
      {showListings && (
        <Box p={2}>
          <ListingSection
            listings={listings}
            onListingClick={handleListingClick}
          ></ListingSection>
        </Box>
      )}
      <ViewListingDialog
        open={viewListingOpen}
        user={user}
        onCloseClick={handleViewListingClose}
        listing={currentListing}
      ></ViewListingDialog>
    </>
  );
}
