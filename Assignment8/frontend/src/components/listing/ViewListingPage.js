import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import CancelIcon from '@mui/icons-material/Cancel';
import {
  AppBar,
  Box,
  Button,
  CardMedia,
  Divider,
  IconButton,
  Stack,
  TextField,
  Toolbar,
} from '@mui/material';
import {grey} from '@mui/material/colors';
import * as AccountUtils from '../../utils/AccountUtils';
import * as ListingApi from '../../dummyapi/ListingApi';
import React from 'react';

/**
 * Function to display the response section
 * @param {*} props the responses
 * @return {*} the reponse section
 */
function ResponseSection(props) {
  const {user, listing} = props;
  const [responses, setResponses] = React.useState([]);
  const isSignedIn = AccountUtils.isSignedIn(user);

  React.useEffect(() => {
    if (isSignedIn) {
      ListingApi.getListingResponseById(listing.id, user.accessToken)
        .then((newResponses) => {
          setResponses(newResponses);
        });
    }
  });

  const responseList = responses.map((response) => (
    <Stack key={response.id} spacing={1}>
      <Stack
        direction="row"
        sx={{color: grey[700], fontSize: 'small', alignItems: 'center'}}
      >
        <AccountCircleIcon />
        <Box component='span' sx={{flexGrow: 1}}>
          {response.fname} {response.lname}
        </Box>
        <Box>11/30</Box>
      </Stack>
      <Box>{response.msg}</Box>
      <Divider />
    </Stack>
  ));

  return <Stack>{responseList}</Stack>;
}

/**
 * Function to display the send message section
 * @param {*} props
 * @return {*} the send message section
 */
function SendMessageSection(props) {
  const {user, listing} = props;
  const [listingResponse, setListingResponse] = React.useState({});

  /**
   * Function to handle data entry
   * @param {*} event data entry
   */
  function handleChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    const newResponse = {...listingResponse};
    newResponse[name] = value;
    setListingResponse(newResponse);
  }

  /**
   * Function to handle send click
   * @param {*} event send click
   */
  function handleSendClick(event) {
    const response = {
      listing_id: listing.id,
      username: user.username,
      msg: listingResponse.msg,
    };
    ListingApi.addListingResponse(response, user.accessToken);
    setListingResponse({});
  }

  return (
    <Stack spacing={2} p={1}>
      <Box>Send seller a message</Box>
      <TextField
        name="msg"
        label="Message"
        variant="outlined"
        value={listingResponse.msg}
        onChange={handleChange} />
      <Button
        variant="contained"
        onClick={handleSendClick}
      >Send</Button>
    </Stack>
  );
}

/**
 * Function to display the picture section
 * @param {*} props
 * @return {*} the picture section
 */
function PictureSection(props) {
  const {listing} = props;
  return (
    <Box
      sx={{
        backgroundColor: grey[300],
        p: 2,
        display: 'flex',
        flexDirection: 'column',
        alignContent: 'center',
        alignItems: 'center',
      }}
    >
      <CardMedia
        component="img"
        image={listing.img}
        sx={{
          maxHeight: {xs: 300, md: 1000},
          maxWidth: {xs: 300, md: 1000},
        }}
      ></CardMedia>
    </Box>
  );
}

/**
 * Function to display the info section of the listing
 * @param {*} props the user and listing
 * @return {*} returns the info section
 */
function InfoSection(props) {
  const {user, listing} = props;

  console.log('user', user);
  console.log('listing', listing);

  const isSignedIn = AccountUtils.isSignedIn(user);
  const showSend = isSignedIn && user.username !== listing.username;
  const showResp = isSignedIn && user.username === listing.username;

  return (
    <Stack spacing={1} p={2}>
      <Box sx={{fontSize: '25px', fontWeight: 'bold'}}>{listing.title}</Box>
      <Box>${listing.price}</Box>
      <Box sx={{fontSize: 'small', color: grey[700]}}>
        Listed about an hour ago in {listing.city}, {listing.stat}
      </Box>
      <Box sx={{fontWeight: 'bold'}}>About This Item</Box>
      <Stack direction="row" spacing={2}>
        <Box sx={{fontWeight: 'bold'}}>Condition</Box>
        <Box>{listing.cond}</Box>
      </Stack>
      <Divider />
      <Box sx={{fontWeight: 'bold'}}>Seller's Description</Box>
      <Box>{listing.descript}</Box>
      <Box sx={{fontWeight: 'bold'}}>{listing.city}, {listing.stat}</Box>
      <Divider />
      {showSend &&
        <SendMessageSection
          user={user}
          listing={listing}
        ></SendMessageSection>}
      {showResp &&
        <ResponseSection listing={listing} user={user}></ResponseSection>}
    </Stack>
  );
}

/**
 * Function to display the toolbar section
 * @param {*} props on close click
 * @return {*} the toolbar section
 */
function ToolbarSection(props) {
  const {onCloseClick} = props;
  return (
    <AppBar position="static" color="fbwhite">
      <Toolbar>
        <IconButton
          size="large"
          aria-label="Close"
          onClick={onCloseClick}
          data-testid="closeViewListing">
          <CancelIcon />
        </IconButton>
      </Toolbar>
    </AppBar>
  );
}

/**
 * Function to display the entire view listing page
 * @param {*} props listing, onCloseClick, user
 * @return {*} the view listing page in its entirety
 */
export default function ViewListingPage(props) {
  const {listing, onCloseClick, user} = props;

  return (
    <Box>
      <ToolbarSection onCloseClick={onCloseClick}></ToolbarSection>
      <Stack direction="row">
        <Box sx={{flexGrow: 1}}>
          <Box>
            <PictureSection listing={listing}></PictureSection>
          </Box>
          <Box display={{xs: 'block', md: 'none'}}>
            <InfoSection listing={listing} user={user}></InfoSection>
          </Box>
        </Box>
        <Box sx={{width: 350}} display={{xs: 'none', md: 'block'}}>
          <InfoSection listing={listing} user={user}></InfoSection>
        </Box>
      </Stack>
    </Box>
  );
}
