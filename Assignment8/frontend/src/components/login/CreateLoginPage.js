import React from 'react';
import {Box, Button, Divider, Paper, Stack, TextField} from '@mui/material';
import {grey} from '@mui/material/colors';
import * as ObjectUtils from '../../utils/ObjectUtils';

/**
 * Function to display the data entry section
 * @param {*} props onChange
 * @return {*} returns the data entry section
 */
function DataEntrySection(props) {
  const {onChange} = props;
  return (
    <Stack spacing={2}>
      <Stack direction="row" spacing={2}>
        <TextField
          name="fname"
          label="First name"
          variant="outlined"
          id="fname"
          onChange={onChange}
        />
        <TextField
          name="lname"
          label="Last name"
          variant="outlined"
          id="lname"
          onChange={onChange}
        />
      </Stack>
      <TextField
        name="username"
        label="Email or phone number"
        variant="outlined"
        id="username"
        onChange={onChange}
      />
      <TextField
        name="passwordhash"
        label="New password"
        variant="outlined"
        type="password"
        id="passwordhash"
        onChange={onChange}
      />
    </Stack>
  );
}

/**
 * Function to display the input section for login
 * @param {*} props onSignUpClick
 * @return {*} returns the input section for login
 */
function InputSection(props) {
  const {onSignUpClick} = props;
  return (
    <Paper>
      <Stack sx={{width: 400}} spacing={2} p={2}>
        <Box sx={{fontSize: '25px', fontWeight: 'bold', textAlign: 'center'}}>
        Create a new account
        </Box>
        <Box sx={{textAlign: 'center', color: grey[700]}}>
        It's quick and easy
        </Box>
        <Divider />
        <DataEntrySection {...props}></DataEntrySection>
        <Button
          variant="contained"
          size="large"
          color="fbgreen"
          sx={{textTransform: 'none'}}
          onClick={onSignUpClick}
          data-testid="SignUp"
        >
        Sign Up
        </Button>
      </Stack>
    </Paper>
  );
}

/**
 * Function to display the create login page
 * @param {*} props onSignUp
 * @return {*} returns the create login page
 */
export default function CreateLoginPage(props) {
  const {onSignUp} = props;
  const [newUser, setNewUser] = React.useState({});

  /**
   * Function to handle the data entry
   * @param {*} event data entry
   */
  function handleChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    const nu = {...newUser};
    nu[name] = value;
    setNewUser(nu);
  }

  /**
   * Function to handle the sign up click
   * @param {*} event sign up click
   */
  function handleSignUpClick(event) {
    console.log('New User', newUser);
    if (ObjectUtils.isNotEmpty(onSignUp)) {
      onSignUp(newUser);
    }
  }

  return (
    <form onSubmit={handleSignUpClick}>
      <Stack
        sx={{
          backgroundColor: grey[200],
          flexGrow: 1,
          alignItems: 'center',
        }}
        p={4}
      >
        <img src="Facebook-Logo.png" style={{width: 180}} alt="Facebook"></img>
        <InputSection
          onChange={handleChange}
          onSignUpClick={handleSignUpClick}
        ></InputSection>
      </Stack>
    </form>
  );
}
