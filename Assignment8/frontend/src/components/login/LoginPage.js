import {Box, Button, Divider, Paper, Stack, TextField} from '@mui/material';
import {grey} from '@mui/material/colors';
import React from 'react';
import {useHistory} from 'react-router-dom';
import * as ObjectUtils from '../../utils/ObjectUtils';

/**
 * Function to display the login section
 * @param {*} props onLogInClick, onChange
 * @return {*} returns the login section
 */
function LoginSection(props) {
  const {onLogInClick, onChange} = props;
  return (
    <Stack spacing={2}>
      <Box
        sx={{textAlign: 'center', fontSize: 'large', fontWeight: 'medium'}}
      >
        Log into Facebook
      </Box>
      <TextField
        name="username"
        label="Email or phone number"
        id="username-input"
        variant="outlined"
        onChange={onChange}
      />
      <TextField
        name="password"
        label="Password"
        id="password-input"
        variant="outlined"
        type="password"
        onChange={onChange}
      />
      <Button
        variant="contained"
        size="large"
        sx={{textTransform: 'none'}}
        data-testid='LoginButton'
        onClick={onLogInClick}
      >
        Log In
      </Button>
    </Stack>
  );
}

/**
 * Function to display the input section for login page
 * @param {*} props onCreateClick
 * @return {*} returns the input section
 */
function InputSection(props) {
  const {onCreateClick} = props;
  return (
    <form onSubmit={onCreateClick}>
      <Paper>
        <Stack sx={{width: 400}} spacing={2} p={2}>
          <LoginSection {...props}></LoginSection>
          <Box sx={{textAlign: 'center'}}>Forgot account?</Box>
          <Divider />
          <Button
            variant="contained"
            size="large"
            color="fbgreen"
            sx={{textTransform: 'none'}}
            onClick={onCreateClick}
            data-testid="CreateNewAccount"
          >
          Create new account
          </Button>
        </Stack>
      </Paper>
    </form>
  );
}

/**
 * Function to display the entire login page
 * @param {*} props onLogin
 * @return {*} returns the entire login page
 */
export default function LoginPage(props) {
  const {onLogin} = props;

  const [loginUser, setLoginUser] = React.useState({});
  const history = useHistory();

  /**
   * Function to handle the data entry
   * @param {*} event data entry
   */
  function handleChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    const nu = {...loginUser};
    nu[name] = value;
    setLoginUser(nu);
  }

  /**
   * Function to handle the login click
   * @param {*} event the login click
   */
  function handleLogInClick(event) {
    if (ObjectUtils.isNotEmpty(onLogin)) {
      onLogin(loginUser);
    }
  }

  /**
   * Function to navigate to create user page
   * @param {*} event create account click
   */
  function handleCreateClick(event) {
    history.push('/account');
  }

  return (
    <Stack
      sx={{
        backgroundColor: grey[200],
        flexGrow: 1,
        alignItems: 'center',
      }}
      p={4}
    >
      <img src="Facebook-Logo.png" style={{width: 180}} alt="Facebook"></img>
      <InputSection
        onChange={handleChange}
        onLogInClick={handleLogInClick}
        onCreateClick={handleCreateClick}
      ></InputSection>
    </Stack>
  );
}
