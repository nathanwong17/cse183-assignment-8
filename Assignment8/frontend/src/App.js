import {BrowserRouter as Router} from 'react-router-dom';
import MarketplaceApp from './components/MarketplaceApp';

/**
 * Simple component with no state.
 *
 * @return {object} JSX
 */
function App() {
  return (
    <Router>
      <MarketplaceApp />
    </Router>
  );
}

export default App;


/**
 * import React from 'react';
  import { BrowserRouter, Route, Switch } from 'react-router-dom';

  import Home from './Home';
  import Login from './Login';
 * Original Setup for App.js
 * <BrowserRouter>
      <Switch>
        <Route path="/" exact>
          <Home/>
        </Route>
        <Route path="/login">
          <Login/>
        </Route>
      </Switch>
    </BrowserRouter>
 *
 * When user moves to login page, we should just do it as he
 * does it in the Authetication Book Example,
 *
 * <Route path="/login">
 * <Login/>
 * </Route>
 *
 * import Login from './Login';
 */
