import {handleResponse, handleError} from './apiUtils';

/**
 * Function to log in
 * @param {*} accountInfo the info for the account
 * @return {*} returns whether login was successful
 */
export function login(accountInfo) {
  return fetch('http://localhost:3010/v0/auth', {
    method: 'POST',
    headers: {'content-type': 'application/json'},
    body: JSON.stringify(accountInfo),
  })
    .then(handleResponse)
    .catch(handleError);
}

/**
 * Function to create an account
 * @param {*} account the account to create
 * @return {*} the newly created account
 */
export function create(account) {
  return fetch('http://localhost:3010/v0/users', {
    method: 'POST',
    headers: {'content-type': 'application/json'},
    body: JSON.stringify({...account, email: account.username}),
  })
    .then(handleResponse)
    .catch(handleError);
}
