import {handleResponse, handleError} from './apiUtils';

/**
 * Function to get the selected category
 * @param {*} category
 * @return {*} the selected category
 */
export function getListings(category) {
  let path = encodeURIComponent('/marketplace');
  if (category !== undefined && category !== null) {
    path = encodeURIComponent(category);
  }
  const baseUrl = `http://localhost:3010/v0/listings?catagory=${path}`;
  return fetch(baseUrl)
    .then(handleResponse)
    .catch(handleError);
}

/**
 * Function to get Listings by User Id
 * @param {*} username the id of the user
 * @param {*} accessToken the access token created by logging in
 * @return {*} returns the listings of the user
 */
export function getListingByUser(username, accessToken) {
  const u = encodeURIComponent(username);
  const baseUrl = `http://localhost:3010/v0/listings/users?username=${u}`;
  return fetch(baseUrl, {
    method: 'GET',
    headers: {'Authorization': `Bearer ${accessToken}`},
  })
    .then(handleResponse)
    .catch(handleError);
}

/**
 * Function to get listing by search words
 * @param {*} words the search words
 * @return {*} the listings by search
 */
export function getListingBySearch(words) {
  const s = encodeURIComponent(words);
  const baseUrl = `http://localhost:3010/v0/listings/search?words=${s}`;
  return fetch(baseUrl)
    .then(handleResponse)
    .catch(handleError);
}

/**
 * Function to add listing
 * @param {*} listing the listing to add
 * @param {*} accessToken the access token created by logging in
 * @return {*} the newly created listing
 */
export function addListing(listing, accessToken) {
  return fetch('http://localhost:3010/v0/listings', {
    method: 'POST',
    headers: {'content-type': 'application/json',
      'Authorization': `Bearer ${accessToken}`},
    body: JSON.stringify(listing),
  })
    .then(handleResponse)
    .catch(handleError);
}

/**
 * Function to post listing response
 * @param {*} response the listing response to post
 * @param {*} accessToken the access token created by logging in
 * @return {*} the newly created listing
 */
export function addListingResponse(response, accessToken) {
  return fetch('http://localhost:3010/v0/listings/response', {
    method: 'POST',
    headers: {'content-type': 'application/json',
      'Authorization': `Bearer ${accessToken}`},
    body: JSON.stringify(response),
  })
    .then(handleResponse)
    .catch(handleError);
}

/**
 * Function to get listing responses by id
 * @param {*} id the id to get
 * @param {*} accessToken the access token created by logging in
 * @return {*} the listing responses
 */
export function getListingResponseById(id, accessToken) {
  const baseUrl = `http://localhost:3010/v0/listings/response/${id}`;
  return fetch(baseUrl, {
    method: 'GET',
    headers: {'Authorization': `Bearer ${accessToken}`},
  })
    .then(handleResponse)
    .catch(handleError);
}
