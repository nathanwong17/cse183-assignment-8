import {handleResponse, handleError} from './apiUtils';

// this is a dummy api to test functionality of buttons in UI
// const marketplaceCategory = {
//   path: [{id: 'marketplace', desc: 'Marketplace', path: '/marketplace'}],
//   children: [
//     {id: 'boats', desc: 'Vehicles', path: '/marketplace/vehicles'},
//     {id: 'boats', desc: 'Property Rentals', path: '/marketplace/vehicles'},
//     {id: 'boats', desc: 'Apparel', path: '/marketplace/vehicles'},
//     {id: 'boats', desc: 'Classifieds', path: '/marketplace/vehicles'},
//   ],
// };

// const vehiclesCategory = {
//   path: [
//     {id: 'marketplace', desc: 'Marketplace', path: '/marketplace'},
//     {id: 'vehicles', desc: 'Vehicles', path: '/marketplace/vehicles'},
//   ],

//   children: [
//     {id: 'boats', desc: 'Boats', path: '/marketplace/vehicles/boats'},
//     {id: 'cars', desc: 'Cars', path: '/marketplace/vehicles/cars'},
//     {
//       id: 'motorcycles',
//       desc: 'Motorcycles',
//       path: '/marketplace/vehicles/motorcycles',
//     },
//     {
//       id: 'powersportvehicles',
//       desc: 'Powersport Vehicles',
//       path: '/marketplace/vehicles/powersportvehicles',
//     },
//   ],
// };

// const searchPhrases = [
//   'corolla',
//   'corolla toyota',
//   'corolla camery',
//   'corolla something',
//   'corolla car',
// ];

// const carsCategory = {
//   path: [
//     {id: 'marketplace', desc: 'Marketplace', path: '/marketplace'},
//     {id: 'vehicles', desc: 'Vehicles', path: '/marketplace/vehicles'},
//     {id: 'cars', desc: 'Cars', path: '/marketplace/vehicles/cars'},
//   ],

//   children: [
//     {
//       id: 'boats',
//       desc: 'Convertibles',
//       path: '/marketplace/vehicles/cars/convertibles',
//     },
//     {
//       id: 'boats',
//       desc: 'Coupes',
//       path: '/marketplace/vehicles/cars/convertibles',
//     },
//     {
//       id: 'boats',
//       desc: 'Hatchbacks',
//       path: '/marketplace/vehicles/cars/convertibles',
//     },
//     {
//       id: 'boats',
//       desc: 'Minivans',
//       path: '/marketplace/vehicles/cars/convertibles',
//     },
//     {
//       id: 'boats',
//       desc: 'SUVs',
//       path: '/marketplace/vehicles/cars/convertibles',
//     },
//   ],
// };

/**
 * Function to get the selected category
 * @param {*} category
 * @return {*} the selected category
 */
export function getCategory(category) {
  // return new Promise((resolve, reject) => {
  //   if (ObjectUtils.isEmpty(category) || category.path === '/marketplace') {
  //     console.log('getcat marketplace');
  //     resolve(marketplaceCategory);
  //   } else if (category.path === '/marketplace/vehicles') {
  //     console.log('getcat vehicles');
  //     resolve(vehiclesCategory);
  //   } else {
  //     console.log('getcat cars', category);
  //     resolve(carsCategory);
  //   }
  // });
  let path = encodeURIComponent('/marketplace');
  if (category !== undefined && category !== null) {
    path = encodeURIComponent(category.cat_path);
  }
  const baseUrl = `http://localhost:3010/v0/category?path=${path}`;
  return fetch(baseUrl)
    .then(handleResponse)
    .catch(handleError);
}

/**
 * http://localhost:3010/v0/category/search?words=f
 * Function to get phrases to match user input
 * @param {*} search the search phrase
 * @return {*} the phrases matching the search
 */
export function getSearchPhrases(search) {
  // let path = encodeURIComponent('/marketplace');
  const s = encodeURIComponent(search);
  const baseUrl = `http://localhost:3010/v0/category/search?words=${s}`;
  return fetch(baseUrl)
    .then(handleResponse)
    .catch(handleError);
}
