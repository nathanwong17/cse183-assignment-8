import * as ObjectUtils from './ObjectUtils';

/**
 * Check if the user has signed in.
 * If the user has an accessToken, that means
 * a successful sign-in.
 * @param {*} user
 * @return {*} true if user is signed in, false otherwise
 */
export function isSignedIn(user) {
  return ObjectUtils.isPropNotEmpty(user, 'accessToken');
}
