/**
 * Function to check if object is empty
 * @param {*} obj
 * @return {*} true if empty, false if not
 */
export function isEmpty(obj) {
  return obj === undefined || obj === null;
}

/**
 * Function to see if object is empty or not
 * @param {*} obj
 * @return {*} true if not empty, false if not
 */
export function isNotEmpty(obj) {
  return obj !== undefined && obj !== null;
}

/**
 * Function to handle empty or not
 * @param {*} obj
 * @param {*} defaultValue
 * @return {*} returns the object if not empty, default value if empty
 */
export function ifEmpty(obj, defaultValue) {
  if (isNotEmpty(obj)) {
    return obj;
  } else {
    return defaultValue;
  }
}

/**
 * Function to handle if string is empty
 * @param {*} str the string to check
 * @param {*} defaultValue
 * @return {*} if empty, return default value. retuns the string
 */
export function ifStringEmpty(str, defaultValue) {
  if (isNotEmpty(str) && str.length > 0) {
    return str;
  } else {
    return defaultValue;
  }
}

/**
 * Function to check if string is empty
 * @param {*} str the string to check
 * @return {*} returns true if empty, false if not
 */
export function isStringEmpty(str) {
  return isEmpty(str) || str.length === 0;
}

/**
 * Function to check if string is not empty
 * @param {*} str the string to check
 * @return {*} true if not empty, false otherwise
 */
export function isStringNotEmpty(str) {
  return isNotEmpty(str) && str.length > 0;
}

/**
 * Function to check if object property field is empty or not
 * @param {*} obj the object to check
 * @param {*} name the name if the property field
 * @return {*} true if not empty, false otherwise
 */
export function isPropNotEmpty(obj, name) {
  return isNotEmpty(obj) && isNotEmpty(name) && isNotEmpty(obj[name]);
}
